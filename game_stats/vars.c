#include "lib.h"

message_buffer buf = {};
olwin olproc = { .chat_address = 0x00534228, .lobby_address = 0x005CA2BC, .running = false };

char *phrases[20] = { 
    "You killed yourself!",
    "You fell to your death!",
    "You died!",
    "You drowned!",
    "You got crushed!",
    "You blew up!",
    "You blew yourself up!",
    "killed you!",
    "blew you up!",
    "killed",
    "blew up",
    "killed himself!",
    "died from a fall!",
    "fell to his death!",
    "died!",
    "drowned!",
    "got crushed!",
    "blew up!",
    "blew himself up!",
    "self-destructed!"
};