#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include "lib.h"

// must sudo sysctl -w kernel.yama.ptrace_scope=0
// sysctl -w kernel.yama.ptrace_scope=1 when done
// gcc -Wall main.c lib.c parser.c vars.c -o game_stats

void initialize_buffers()
{
    memset(buf.last_message, '\0', CHAT_BUFFER);
    memset(buf.player_list, '\0', PLAYER_LIST);
    memset(buf.score_buffer, '\0', FINAL_BUFFER);
    memset(buf.final_player_list, '\0', FINAL_BUFFER);
}

int main(int argc, char **argv)
{
    attach_olwin(argv);
    initialize_buffers();

    while(olproc.running) {
        process_game_chat_string();
        read_player_list();
        olproc.running = is_outlaws_running();
    }
    
    output("debug:game-over", true);
    output("score:%s", true, buf.score_buffer);
    output("players:%s", true, buf.final_player_list);

    return 1;
}