#define _GNU_SOURCE
#include <sys/uio.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>
#define BUFFER_LENGTH 128
#define PLAYER_LIST 2600
// must sudo sysctl -w kernel.yama.ptrace_scope=0
// sysctl -w kernel.yama.ptrace_scope=1 when done

typedef struct {
    struct iovec lMessages[1];
    struct iovec rMessages[1];
    struct iovec lPlayers[1];
    struct iovec rPlayers[1];
} addresses_t;

typedef struct {
    char *lastMessage;
    char playerList[PLAYER_LIST];
} messageBuffer;

struct stat sts;

messageBuffer buf = {};
addresses_t addresses = {};

char *phrases[] = { 
    "You killed yourself!",
    "You fell to your death!",
    "You died!",
    "You drowned!",
    "You got crushed!",
    "You blew up!",
    "You blew yourself up!",
    "killed you!",
    "blew you up!",
    "killed",
    "blew up",
    "killed himself!",
    "died from a fall!",
    "fell to his death!",
    "died!",
    "drowned!",
    "got crushed!",
    "blew up!",
    "blew himself up!",
    "self-destructed!"
};

void setToNull(char *str) {
    memset(str, '\0', strlen(str));
}

bool isNewMessage(char *read, char *buffer) {
    bool retval = false;
    if(strlen(read) == 0) {
        setToNull(buffer);
    }
    else {        
        if(strcmp(buffer, read) != 0) {
            retval = true;
        }
    }

    return retval;
}

bool isChatMessage(char *read) {
    char *ret = strstr(read, ":");
    return ret != NULL;
}

bool messageAffectsScore(char *read) {
    bool affectsScore = false;
    int size = sizeof(phrases)/ sizeof(phrases[0]);
    for(int i = 0; i < size; i++) {
        char *line = phrases[i];
        char *ret;
        ret = strstr(read, line);
        if(ret != NULL) {
            affectsScore = true;
            break;
        }
    }

    return affectsScore;
}

void ProcessGameChatString(pid_t pid) {
    ssize_t nread = process_vm_readv(pid, addresses.lMessages, 1, addresses.rMessages, 1, 0);
    char *read = addresses.lMessages[0].iov_base;
    bool _isChatMessage = isChatMessage(read);
    bool _isNewMessage = isNewMessage(read, buf.lastMessage);
    bool _affectsScore = false;
    
    if(!_isChatMessage && _isNewMessage) {
        _affectsScore = messageAffectsScore(read);
        if(_affectsScore) {
            strcpy(buf.lastMessage, read);
            printf("score:%s", buf.lastMessage);
            fflush(stdout);
        }
    }
}

void ReadPlayerList(pid_t pid) {
    char buffer[PLAYER_LIST];
    ssize_t nread = process_vm_readv(pid, addresses.lPlayers, 1, addresses.rPlayers, 1, 0);
    memcpy(buffer, addresses.lPlayers[0].iov_base, PLAYER_LIST);
    for(int i = 0; i < PLAYER_LIST; i++) {
        if(buffer[i] == '\0' || buffer[i] <= 32 || buffer[i] == '!' || buffer[i] == ',') {
            buffer[i] = '.';
        }
    }

    buffer[PLAYER_LIST - 1] = '\0';
    if(memcmp(buf.playerList, buffer, PLAYER_LIST) != 0) {  
        memcpy(buf.playerList, buffer, PLAYER_LIST);
        printf("players:%s", buffer);
        fflush(stdout);
    }
}

int main(int argc, char **argv) {
    pid_t pid = atoi(argv[1]);
    buf.lastMessage = (char *)calloc(BUFFER_LENGTH, sizeof(char));
    memset(buf.playerList, '\0', PLAYER_LIST);

    addresses.lMessages[0].iov_base = calloc(BUFFER_LENGTH, sizeof(char));
    addresses.lMessages[0].iov_len = BUFFER_LENGTH;
    addresses.lPlayers[0].iov_base = calloc(PLAYER_LIST, sizeof(char));
    addresses.lPlayers[0].iov_len = PLAYER_LIST;

    addresses.rMessages[0].iov_base = (void *) 0x00534228;
    addresses.rMessages[0].iov_len = BUFFER_LENGTH;
    addresses.rPlayers[0].iov_base = (void *) 0x005CA2BC;
    addresses.rPlayers[0].iov_len = PLAYER_LIST;

    char *cmd = (char *)calloc(20, sizeof(char));
    sprintf(cmd, "/proc/%li", pid);

    while(1) {
        ProcessGameChatString(pid);
        ReadPlayerList(pid);
        if(stat(cmd, &sts) == -1 && errno == ENOENT) {
            break;
        }
    }

    printf("game-over");
    fflush(stdout);

    return 1;
}

