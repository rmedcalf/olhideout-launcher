#define _GNU_SOURCE
#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include "lib.h"

#ifdef __linux__
    #include <errno.h>
#else
    #include <windows.h>
#endif

void set_to_null(char *str)
{
    memset(str, '\0', strlen(str));
}

bool is_new_message(char *read, char *buffer)
{
    bool is_new = false;

    if(strlen(read) == 0) {
        set_to_null(buffer);
    }
    else if(strcmp(buffer, read) != 0) {
        is_new = true;
    }
    return is_new;
}

bool message_affects_score(char *read)
{
    bool affects_score = false;
    int size = sizeof(phrases)/ sizeof(phrases[0]);
    for(int i = 0; i < size; i++) {
        char *line = phrases[i];
        char *ret = (char *) strstr(read, line);
        if(ret != NULL) {
            affects_score = true;
            break;
        }
    }

    return affects_score;
}

bool is_chat_message(char *read)
{
    char *ret = strstr(read, ":");
    return ret != NULL;
}

bool is_outlaws_running()
{
    #ifdef __linux__
        if(stat(olproc.stat_cmd, &olproc.sts) == -1 && errno == ENOENT) return false;
        return true;
    #else
        GetExitCodeProcess(olproc.handle, &olproc.exitCode);
        return olproc.exitCode == STILL_ACTIVE;    
    #endif
}

void attach_olwin(char **argv)
{
    olproc.pid = atoi(argv[1]);
    #ifdef __linux__
        olproc.l_messages[0].iov_base = calloc(CHAT_BUFFER, sizeof(char));
        olproc.l_messages[0].iov_len = CHAT_BUFFER;
        olproc.l_players[0].iov_base = calloc(PLAYER_LIST, sizeof(char));
        olproc.l_players[0].iov_len = PLAYER_LIST;

        olproc.r_messages[0].iov_base = (void *) olproc.chat_address;
        olproc.r_messages[0].iov_len = CHAT_BUFFER;
        olproc.r_players[0].iov_base = (void *) olproc.lobby_address;
        olproc.r_players[0].iov_len = PLAYER_LIST;
        
        olproc.stat_cmd = (char *)calloc(20, sizeof(char));
        sprintf(olproc.stat_cmd, "/proc/%li", olproc.pid);
    #else
        olproc.handle = OpenProcess(PROCESS_VM_READ | PROCESS_QUERY_INFORMATION, true, olproc.pid);
    #endif

    olproc.running = true;
}

void output(char *format, bool pause, ...)
{
    va_list args;
    va_start(args, pause);
    vfprintf(stdout, format, args);
    va_end(args);
    fflush(stdout);
    #ifdef __linux__
    if(pause) sleep(1);
    #else
    if(pause) Sleep(1000);
    #endif
}