let players = new Set();
let buf = 'debug[players]:RUSSELL.....J';
let player = '';
buf = buf.split('debug[players]:')[1];
for(var i = 0; i < buf.length; i++) {
    var char = buf[i];
    if(char != '.') {
        player += char;
    }
    else {
        if(player.length > 1) {
            players.add(player);
        }
        player = '';
    }
}
console.log(players);
return;


var spawn = require('child_process').spawn;
let proc = spawn('./game_stats', ['3020']);
var playersList = new Set();
var scores = [];
var gameOver = false;
var gameReady = false;

proc.on('exit', () => {
    if(scores.length) {
        let players = Array.from(playersList);
        players = players.reverse();
        scores = scores.reverse();
        var obj = { action: 'game-over-118', messages: scores, playerList: players };
        console.log(obj);
    }
});

proc.stdout.on('data', (response) => {
    let message = response.toString();
    var delimiter = '[|]:';
    if(message) {
        console.log({ message });
        if(message.startsWith('debug[players]:')) {
            if(!message.startsWith('debug[players]:.')) {
                gameReady = true;
                console.log('game ready');
            }
        }
        else if(message.startsWith('score:')) {
            scores = message.split(delimiter);
            scores.shift();
        }
        else if(message.startsWith('players:')) {
            var players = message.split(delimiter);
            players.shift();
            playersList = new Set(players);
        }
        else if(message == 'debug:game-over') {
            gameOver = true;
        }
    }
});