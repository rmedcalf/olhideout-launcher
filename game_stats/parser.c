#define _GNU_SOURCE
#include "lib.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#ifdef __linux__
#include <sys/uio.h>
#else
#include <windows.h>
#endif

#ifdef __linux__
    char *read_linux_memory(unsigned long address, size_t size)
    {
        char *read = NULL;
        if(address == olproc.chat_address) {
            process_vm_readv(olproc.pid, olproc.l_messages, 1, olproc.r_messages, 1, 0);
            read = olproc.l_messages[0].iov_base;
        }
        else if(address == olproc.lobby_address) {
            process_vm_readv(olproc.pid, olproc.l_players, 1, olproc.r_players, 1, 0);
            read = olproc.l_players[0].iov_base;
        }
        
        return read;        
    }
#else
    char *read_windows_memory(unsigned long address, size_t size)
    {
        char *read = (char *)calloc(size, sizeof(char));
        ReadProcessMemory(olproc.handle, (unsigned long *)address, read, size, NULL);
        return read;
    }
#endif

char *read_memory(unsigned long address, size_t size)
{
    #ifdef __linux__
        return read_linux_memory(address, size);
    #else
        return read_windows_memory(address, size);
    #endif
}

void process_game_chat_string()
{
    char *read = read_memory(olproc.chat_address, CHAT_BUFFER);
    if(read == NULL) return;    

    bool is_chat = is_chat_message(read);
    bool is_new = is_new_message(read, buf.last_message);
    bool affects_score = false;

    if(!is_chat && is_new) {        
        affects_score = message_affects_score(read);
        if(affects_score) {
            strcpy(buf.last_message, read);
            char *message = (char *) calloc(strlen(buf.last_message) + 7, sizeof(char));
            sprintf(message, "[|]:%s", buf.last_message);
            strcat(buf.score_buffer, message);
            output("debug[score]:%s", false, buf.last_message);
            free(message);
        }
    }

    #ifndef __linux__
    free(read);
    #endif
}

void read_player_list()
{
    char buffer[PLAYER_LIST];
    char *read = read_memory(olproc.lobby_address, PLAYER_LIST);
    if(read == NULL) return;
    memcpy(buffer, read, PLAYER_LIST);

    for(int i = 0; i < PLAYER_LIST; i++) {
        if(buffer[i] == '\0' || buffer[i] <= 32 || buffer[i] == '!' || buffer[i] == ',') {
            buffer[i] = '.';
        }
    }

    buffer[PLAYER_LIST - 1] = '\0';
    if(memcmp(buf.player_list, buffer, PLAYER_LIST) != 0) {
        memcpy(buf.player_list, buffer, PLAYER_LIST);
        char *message = (char *) calloc(strlen(buf.player_list) + 7, sizeof(char));
        sprintf(message, "[|]:%s", buffer);
        strcat(buf.final_player_list, message);
        output("debug[players]:%s", false, buffer);
        free(message);
    }

    #ifndef __linux__
    free(read);
    #endif
}