#include <sys/stat.h>
#include <stdbool.h>
#define CHAT_BUFFER 128
#define PLAYER_LIST 2600
#define FINAL_BUFFER 600000

#ifdef __linux__
#include <sys/uio.h>
#else
#include <windows.h>
#endif

typedef struct {
    long pid;
    bool running;
    unsigned long chat_address;
    unsigned long lobby_address;
    #ifdef __linux__
    struct stat sts;
    char *stat_cmd;
    struct iovec l_messages[1];
    struct iovec r_messages[1];
    struct iovec l_players[1];
    struct iovec r_players[1];
    #else
    HANDLE handle;
    DWORD exitCode;
    #endif
} olwin;

typedef struct {
    char last_message[CHAT_BUFFER];
    char player_list[PLAYER_LIST];
    char final_player_list[FINAL_BUFFER];
    char score_buffer[FINAL_BUFFER];
} message_buffer;

extern message_buffer buf;
extern olwin olproc;
extern char *phrases[20];
bool is_outlaws_running();
void set_to_null(char *str);
bool is_new_message(char *read, char *buffer);
bool message_affects_score(char *read);
bool is_chat_message(char *read);
void process_game_chat_string();
void read_player_list();
void attach_olwin(char **argv);
void output(char *format, bool pause, ...);
char *read_memory(unsigned long address, size_t size);