; -- Example1.iss --
; Demonstrates copying 3 files and creating an icon.

; SEE THE DOCUMENTATION FOR DETAILS ON CREATING .ISS SCRIPT FILES!

[Setup]
AppName=Outlaws Discord Launcher
AppVersion=1.18
AppPublisher=The Outlaws Hideout
AppPublisherURL=http://www.olhideout.net
DefaultDirName={pf}\Outlaws Discord Launcher
DefaultGroupName=Outlaws Discord Launcher
UninstallDisplayIcon={app}\olhideout-launcher.exe
Compression=lzma2
SolidCompression=yes
OutputDir=C:\launcher
OutputBaseFilename=olhideout-launcher-test

[Files]
Source: "build\win-unpacked\*"; DestDir: "{app}"
Source: "build\win-unpacked\resources\*"; DestDir: "{app}\resources"
Source: "build\win-unpacked\locales\*"; DestDir: "{app}\locales"
Source: "build\win-unpacked\addons\*"; DestDir: "{app}\addons"

[Icons]
Name: "{group}\Outlaws Discord Launcher"; Filename: "{app}\olhideout-launcher.exe"
