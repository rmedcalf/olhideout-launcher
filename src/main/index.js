import { app, Menu, BrowserWindow, ipcMain } from 'electron'
import settings from '../renderer/framework/settings'

const download = require('download-file');
const extract = require('extract-zip');
const ProgressBar = require('electron-progressbar');
const hotkey = require('electron-hotkey');
const log = require('electron-log');
const {autoUpdater} = require("electron-updater");
const isDev = require('electron-is-dev');

if(isDev) {
  autoUpdater.updateConfigPath = require('path').join(__dirname, 'dev-app-update.yml')
}

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`;

function sendStatusToWindow(text) {
  mainWindow.webContents.send('message', { message: text });
}

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 480,
    //useContentSize: true,
    resizable: true,
    width: 1000
  });

  if(settings.maximized) {
    mainWindow.maximize();
  }

  app.setAsDefaultProtocolClient('olhideout-launcher');
  var autoJoin = false;
  
  for(let arg of process.argv) {
    if(arg.startsWith('olhideout-launcher')) {
      settings.autoJoinGame = arg;
      autoJoin = true;
    }
  }

  hotkey.register(mainWindow, 'CommandOrControl+Shift+D', 'toggle-debug');

  if(!autoJoin) settings.autoJoinGame = null;

  mainWindow.loadURL(winURL)
  //mainWindow.webContents.openDevTools();

  mainWindow.on('closed', () => {
    mainWindow = null
  })

  mainWindow.on('maximize', () => {
    settings.maximized = true;
  });

  mainWindow.on('unmaximize', () => {
    settings.maximized = false;
  });
}

function devToolsLog(s) {
  console.log(s)
  if (mainWindow && mainWindow.webContents) {
    mainWindow.webContents.executeJavaScript(`console.log("${s}")`)
  }
}

function getMenuTemplate() 
{
  var template = [{
    label: '&File',
    submenu: [
      { label: '&Host a Game', click: () => { mainWindow.webContents.send('show-component', '/host'); } },
      { label: '&Manual Join', click:() => { mainWindow.webContents.send('show-component', '/manual-join');} },
      { label: '&Settings', click: () => { mainWindow.webContents.send('show-component', '/options'); } },
      { label: '&Quit', accelerator: 'Ctrl+W', click: () => { app.quit(); } }
    ]
  },
  {
    label: '&Help',
    submenu: [
      { label: '&Release Notes', click:() => { mainWindow.webContents.send('show-component', '/'); }},
      { label: 'Reload Window', accelerator: 'Ctrl+R', click:(item, f) => { f.reload(); } }
    ]
  }];

  if(process.env.NODE_ENV == 'development') {
     template.push({
       label: '&Debug',
       submenu: [
         { label: 'Reload', click:(item, focusedWindow) => { focusedWindow.reload(); } },
         { label: 'Toggle Developer Tools', click: (item, focusedWindow) => { focusedWindow.toggleDevTools();} }
       ]
    });
  }

  return template;
}

app.on('ready', () => {
  createWindow();
  var template = getMenuTemplate();
  let menu = Menu.buildFromTemplate(template);
  Menu.setApplicationMenu(menu);

  ipcMain.on('download', (event, arg) => {
    let progressBar = new ProgressBar({
      text: 'Launching Game',
      detail: 'Installing Map...',
      browserWindow: {
        parent: mainWindow
      }
    });
    
    progressBar.on('completed', () => {
      progressBar.detail = 'Map Installed. Launching Game';
    });

    download(arg.url, { filename : 'map.zip', directory: app.getPath('temp') }, (err) => {
      if(err) {
        event.returnValue = 'download-error';
        progressBar.setCompleted();
      }
      
      extract(`${app.getPath('temp')}/map.zip`, { dir: arg.path }, (err) => {
        progressBar.setCompleted();
        if(err) {
          event.returnValue = 'install-error';
        }
        else  {
          event.returnValue = 'success';
        }
      });      
    });
  });
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('shortcut-pressed', (event) => {
  if(event == 'toggle-debug') {
    mainWindow.toggleDevTools();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})


autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall();
})

app.on('ready', () => {
  autoUpdater.checkForUpdates();
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
