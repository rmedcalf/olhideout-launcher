import Vue from 'vue'
import axios from 'axios'
import App from './App'
import router from './router'
import store from './store'
import 'vue-instant/dist/vue-instant.css'
import VueInstant from 'vue-instant'
import settings from './framework/settings'
import moment from 'moment'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.use(VueInstant)
Vue.prototype.$mySettings = settings;

Vue.filter('date', function(value, format = 'MM/DD/YYYY') {
  if(!value) return '';
  return moment(value).format(format);

});

Vue.filter('dateTime', function(value, format = 'MM/DD/YYYY hh:mm a'){
  if(!value) return '';
  return moment(value).format(format);
})

Vue.filter('arrayToString', function(value) {
  if(!value) return '';
  return value.join(', ');
})

axios.get(`${settings.url}/api/launcher/settings`).then(result => {
  settings.socket = result.data.socket_port;
  let v = new Vue({
    components: { App },
    router,
    store,
    template: '<App/>'
  }).$mount('#app')
});
/* eslint-disable no-new */



