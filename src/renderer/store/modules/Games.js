const state = {
    games: [],
    recentGames: []
}
  
const mutations = {
    updateGame(state, game) {
        state.games = [...state.games.filter(g => g._id != game._id)];
        state.games.unshift(game);
    },
    load(state, data) {
        state.games = data.games;
        state.recentGames = data.recentGames;
    },
    add(state, data) {
        if(!state.games.find(g => { return g._id == data._id; })) {
            state.games.unshift(data);
        }
    }
}

const actions = {
    someAsyncTask ({ commit }) {
        // do something async
        commit('INCREMENT_MAIN_COUNTER')
    }
}
  
export default {
    namespaced: true,
    state,
    mutations,
    actions
}
  