const state = {
    game: null,
    active: false,
    stats: []
}

const getters = {
    gameId: state => {
        return state.game ? state.game._id : null;
    }
}
  
const mutations = {
    select(state, data) {
        state.game = data.game;
        state.active = data.active;
    }
}

const actions = {
    someAsyncTask ({ commit }) {
        // do something async
        commit('INCREMENT_MAIN_COUNTER')
    }
}
  
export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
  