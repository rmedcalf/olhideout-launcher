import settings from '../framework/settings'
export default {
    methods: {
        hasScreenshot(obj) {
            let screenshot = this.getScreenshot(obj);
            return screenshot != null;
        },
        getScreenshot(obj) {
            if(obj.map && obj.map.screenshot) {
                return `${settings.url}/screens/${obj.map.screenshot}`;
            }
            else if(obj.screenshot) {
                return `${settings.url}/screens/${obj.screenshot}`;
            }
            return null;
        }
    },
}