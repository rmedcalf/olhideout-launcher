import moment from 'moment'

function sleep(interval) {
    return new Promise((resolve, reject) => {
        setTimeout(() => { resolve(); }, interval);
    });
}

function utcNow() {
    return moment().utc();
}

export { sleep, utcNow }