import { ipcRenderer } from 'electron'
import settings from './settings'

export default function(url) {
    ipcRenderer.sendSync('download', { url, path: settings.mapInstallPath });
}