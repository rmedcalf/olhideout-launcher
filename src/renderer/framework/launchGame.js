import installMap from './installMap'
import settings from './settings'
import { sleep } from './utils'
const tasklist = require('tasklist')
const exec = require('child_process').exec

export default function(options = {}) {
    let mode = options.hosting ? '/host' : '/join';
    let netdriver = '/netdriver lecdp3.dll';
    let nickname = options.hosting ? settings.nickname : `${settings.nickname} ${options.ip}`;
    
    return new Promise(async (resolve, reject) => {
        if(options.url) {
            installMap(options.url);
        }
        else if(options.filename) {
            let url = `${settings.url}/maps/${options.filename}`;
            installMap(url);
        }

        if(process.platform == 'linux') {
            let child = exec(`${settings.olwin} ${mode} ${nickname} ${netdriver}`);
            child.stdout.on('data', async (response) => {
                let data = response.toString();
                if(data && data.startsWith('Running: olwin.exe')) {
                    await sleep(1000);
                    exec('pidof olwin.exe', (err, stdout, stderr) => {
                        let pid = stdout.replace('\r', '').replace('\n','');
                        console.log({ pid });
                        resolve({ stats: options.runStats, pid });
                    });
                }
            });
        }
        else {
            let child = exec(`"${settings.olwin}" ${mode} ${nickname} ${netdriver}`);
            var tasks = await tasklist();
            var olwin = tasks.find(task => {
                return task.imageName == 'olwin.exe';
            });
            await sleep(2000);
            let pid = olwin ? olwin.pid : null;
            console.log({ pid });
            resolve({ stats: options.runStats && pid, pid });
        }
    });
}
