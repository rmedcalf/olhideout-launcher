const uuidv1 = require('uuid/v1');
import settings from './settings'
import { onRefreshGamesList, onNewGame, onGameUpdate } from './events';

var actions = new Map([
    ['refresh-games-list', onRefreshGamesList ],
    ['new-game', onNewGame ],
    ['game-update', onGameUpdate ]
]);

class LauncherSocket {
    constructor() {
        this.timerId = 0;
        this.clientId = uuidv1();
    }

    debug(data, message) {
        data.clientId = this.clientId;
        data.action = 'debug';
        data.message = message;
        this.send(data);
    }

    sendChat(message) {
        this.send({ action: 'say', message, channel: 'general' });
    }

    send(data) {
        data.clientId = this.clientId;
        console.log('sending', data);
        this.ws.send(JSON.stringify(data));
    }

    connect() {
        this.ws = new WebSocket(settings.socket);
        this.ws.onopen = () => {
            this.clearTimer();
            this.send({ action: 'client-id-init', clientId: this.clientId });
            this.send({ action: 'refresh-games-list' });
            this.send({ action: 'client-id-test' });
        }

        this.ws.onclose = () => { this.tryReconnect(); }
        this.ws.onmessage = (event) => {
            if(event && event.data) {
                let message = JSON.parse(event.data);
                console.log('received', message);
                if(actions.has(message.action)) {
                    actions.get(message.action)(message);
                }
            }
        }
    }

    tryReconnect() {
        if(!this.timerId) {
            this.timerId = setInterval(() => {
                this.connect();
            }, 5000);
        }
    }

    clearTimer() {
        if(this.timerId) {
            clearInterval(this.timerId);
            this.timerId = 0;
        }
    }
}

const socket = new LauncherSocket();
export default socket;