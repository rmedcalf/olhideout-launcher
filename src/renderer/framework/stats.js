import socket from './socket'
var spawn = require('child_process').spawn;
var exec = require('child_process').exec;
var olwinListen = false;

export default function(clientGameId, pid) {
    var playersList = new Set();
    var scores = [];
    let players = [];
    var playersIn = [];
    var gameOver = false;
    var gameStarted = false;
    var gameReady = false;
    
    return new Promise((resolve, reject) => {
        let resolved = false;
        if(olwinListen) {
            resolve();
            return;
        }

        let statsPath = '';
        if(process.platform == 'linux') {
            statsPath = './addons/game_stats';
        }
        else {
            statsPath = './addons/game_stats.exe';
        }

        let proc = spawn(statsPath, [pid]);
        olwinListen = true;

        proc.on('exit', () => {
            olwinListen = false;
            if(scores.length) {
                let players = Array.from(playersList);
                players = players.reverse();                
                scores = scores.reverse();
                var obj = { action: 'game-over-118', messages: scores, playerList: players, clientGameId };
                socket.send(obj);
                resolve(obj);
                resolved = true;
            }
        });

        proc.stdout.on('data', (response) => {
            let message = response.toString();
            var delimiter = '[|]:';
            if(message) {
                console.log( { message });
                if(message.startsWith('debug[players]:')) {
                    if(!message.startsWith('debug[players]:.')) {
                        players.push(message);
                        //socket.send({ action: 'game-players', data: message, clientGameId, delimiter });
                        if(!gameReady) {
                            gameReady = true;
                            socket.sendChat('Game is up!');
                        }
                        else if(!gameStarted) {
                            let buf = message;
                            let player = '';
                            buf = buf.split('debug[players]:')[1];
                            for(var i = 0; i < buf.length; i++) {
                                var char = buf[i];
                                if(char != '.') {
                                    player += char;
                                }
                                else {
                                    if(player.length > 1 && !playersIn.includes(player)) {
                                        playersIn.push(player);
                                        socket.sendChat(`${player} has joined the game!`);
                                    }
                                    player = '';
                                }
                            }
                        }
                    }
                }
                else if(message.startsWith('debug[score]:')) {
                    //socket.send({ action: 'game-score', data: message, clientGameId, delimiter });
                    if(!gameStarted) {
                        socket.sendChat('Game has started!');
                        gameStarted = true;
                    }
                }
                else if(message.startsWith('score:')) {
                    scores = message.split(delimiter);
                    scores.shift();
                }
                else if(message.startsWith('players:')) {
                    players = players.map(p => {
                        let parts = p.split('debug[players]:');
                        return parts[1];
                    });
                    playersList = new Set(players);
                    // var players = message.split(delimiter);
                    // players.shift();
                    // playersList = new Set(players);
                }
                else if(message == 'debug:game-over') {
                    gameOver = true;
                }
            }
        });
    });
}