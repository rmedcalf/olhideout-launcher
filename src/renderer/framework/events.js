import store from '../store/index'
import settings from './settings'
import moment from 'moment'
import { utcNow } from './utils';

export function onGameUpdate(message) {
    store.commit('Games/updateGame', message.game);
}

export function onRefreshGamesList(message) {
    let recentGames = [];
    let games = [];
    if(message.games) {
        message.games.forEach(game => {
            var createdOn = moment(game.createdOn).utc();
            var minutesAgo = utcNow().diff(createdOn, 'minutes');
            if(minutesAgo > settings.minutes) {
                recentGames.push(game);
            }
            else {
                games.push(game);
            }
        });
    }
    store.commit('Games/load', { games, recentGames });
}

export function onNewGame(message) {
    //mapGameInfo(message.game);
    store.commit('Games/add', message.game);
}

/// to show game stats in game info window
export function echoStats(message) {

}