const settings = require('electron-settings');
const localUrl = '192.168.0.105';
const remoteUrl = 'olhideout.net';

class LauncherSettings {
    constructor() {
        this.defaults = {
            url: 'http://olhideout.net',
            maximized: false,
            nickname: 'Stranger',
            path: "C:\\GOG Games\\Outlaws",
            newGameNotifications: false,
            minutes: 20,
            autoJoinGame: null
        };
    }

    getValue(prop, def) {
        return settings.get(prop) || def;
    }

    get maximized() {
        return this.getValue('maximized', false);
    }

    set maximized(value){
        settings.set('maximized', { value });
    }

    get autoJoinGame() {
        return this.getValue('autoJoinGame.value', this.defaults.autoJoinGame);
    }

    set autoJoinGame(value) {
        settings.set('autoJoinGame', { value });
    }

    get newGameNotifications() {
        return this.getValue('newGameNotifications.value', this.defaults.newGameNotifications);
    }

    set newGameNotifications(value) {
        settings.set('newGameNotifications', { value });
    }

    get mapInstallPath() {
        if(process.platform == 'linux') {
            return `${this.path}/gamedir`;
        }

        return this.path;
    }

    get olwin() {
        if(process.platform == 'linux') {
            return `${this.path}/start.sh play :`;
        }

        return `${this.path}\\olwin.exe`
    }

    get path() {
        if(process.platform == 'linux') {
            //return '/home/russell/Desktop/Outlaws/Outlaws/start.sh play :';
            return '/home/russell/outlaws_wine/Outlaws';
        }
        return this.getValue('path.value', this.defaults.path);
    }

    set path(value) {
        settings.set('path', { value });
    }

    get url() {
        if(process.env.NODE_ENV == 'development') {
            return `http://${localUrl}:9000`;
        }
        return this.getValue('url.value', this.defaults.url);
    }

    get socket() {
        let value = this.getValue('socket.value', this.defaults.socket);
        if(process.env.NODE_ENV == 'development') {
            return `ws://${localUrl}:${value}`
        }
        else {
            return `ws://${remoteUrl}:${value}`;
        }
    }

    set socket(value) {
        settings.set('socket', { value });
    }

    set url(value) {
        settings.set('url', { value });
    }

    get minutes() {
        return this.getValue('minutes.value', this.defaults.minutes);
    }

    set minutes(value) {
        settings.set('minutes', { value });
    }

    get nickname() {
        return this.getValue('nickname.value', this.defaults.nickname);
    }

    set nickname(value) {
        settings.set('nickname', { value });
    }
}

const launcherSettings = new LauncherSettings();
export default launcherSettings;