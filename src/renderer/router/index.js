import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
import Options from '../components/Options'
import GameInfo from '../components/GameInfo'
import ReleaseNotes from '../components/ReleaseNotes'
import HostGame from '../components/HostGame'
import ManualJoin from '../components/ManualJoin'

export default new Router({
  routes: [
    { path: '/', name: 'release-notes', component: ReleaseNotes },
    { path: '/host', name: 'host', component: HostGame },
    { path: '/manual-join', name: 'manual-join', component: ManualJoin },
    { path: '/game', name: 'game', component: GameInfo },
    { path: '/options', name: 'options', component: Options },
    { path: '/default-page', name: 'landing-page', component: require('@/components/LandingPage').default },
    { path: '*', redirect: '/' }
  ]
})
