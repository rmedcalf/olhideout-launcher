import { ipcRenderer } from 'electron'
import settings from './renderer/framework/settings'
import statsService from './statsService'
const exec = require('child_process').exec

export class MapLauncher {
    constructor(url, ip, cdMap = false) {
        this.url = url;
        this.cdMap = cdMap;
        this.ip = ip;
    }

    installMap() {
        if(!this.cdMap) {
            ipcRenderer.sendSync('download', { url: this.url, path: settings.mapInstallPath });
        }
    }

    hostGame(nickname, socket) {
        settings.nickname = nickname;
        this.installMap();
        socket.eventHub.on('new-game-success', (data) => {
            if(process.platform == 'linux') {
                exec(`${settings.olwin} /host ${settings.nickname} /netdriver lecdp3.dll`);
            }
            else {
                exec(`"${settings.olwin}" /host ${settings.nickname} /netdriver lecdp3.dll`);
                setTimeout(() => {
                    statsService(data.game, socket, nickname);
                }, 6000);
            }
            socket.eventHub.clear('new-game-success');
        });
    }

    joinGame() {
        this.installMap();
        if(process.platform == 'linux') {                    
            exec(`${settings.olwin} /join ${settings.nickname} ${this.ip} /netdriver lecdp3.dll`);
        }
        else {
            exec(`"${settings.olwin}" /join ${settings.nickname} ${this.ip} /netdriver lecdp3.dll`);
        }
    }
}